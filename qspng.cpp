#include "qspng.h"
#include <QIODevice>
#include <QImage>
#include <QDebug>
#include <QColorSpace>
#include <QElapsedTimer>
#include <QFile>
#include <QtMath>

extern "C" {
#include <spng.h>
}

Q_LOGGING_CATEGORY(LOGGING, "imageformats.spng", QtWarningMsg)

QDebug operator<<(QDebug debug, const spng_color_type type) {
    QDebugStateSaver saver(debug);
    const char *name = nullptr;

    switch(type) {
    case SPNG_COLOR_TYPE_GRAYSCALE: name = "SPNG_COLOR_TYPE_GRAYSCALE"; break;
    case SPNG_COLOR_TYPE_TRUECOLOR: name = "SPNG_COLOR_TYPE_TRUECOLOR"; break;
    case SPNG_COLOR_TYPE_INDEXED: name = "SPNG_COLOR_TYPE_INDEXED"; break;
    case SPNG_COLOR_TYPE_GRAYSCALE_ALPHA: name = "SPNG_COLOR_TYPE_GRAYSCALE_ALPHA"; break;
    case SPNG_COLOR_TYPE_TRUECOLOR_ALPHA: name = "SPNG_COLOR_TYPE_TRUECOLOR_ALPHA"; break;
    default: break;
    }
    if (name) {
        debug.nospace() << name;
    } else {
        debug.nospace() << "spng_color_type(" << int(type) << ')';
    }

    return debug;
}

QDebug operator<<(QDebug debug, const spng_format format) {
    QDebugStateSaver saver(debug);

    const char *name = nullptr;
    switch(format) {
    case SPNG_FMT_RGBA8: name = "SPNG_FMT_RGBA8"; break;
    case SPNG_FMT_RGBA16: name = "SPNG_FMT_RGBA16"; break;
    case SPNG_FMT_RGB8: name = "SPNG_FMT_RGB8"; break;
    case SPNG_FMT_GA8: name = "SPNG_FMT_GA8"; break;
    case SPNG_FMT_GA16: name = "SPNG_FMT_GA16"; break;
    case SPNG_FMT_G8: name = "SPNG_FMT_G8"; break;
    case SPNG_FMT_PNG: name = "SPNG_FMT_PNG"; break;
    case SPNG_FMT_RAW: name = "SPNG_FMT_RAW"; break;
    default: break;
    }
    if (name) {
        debug.nospace() << name;
    } else {
        debug.nospace() << "spng_format(" << int(format) << ')';
    }

    return debug;
}

static int qiodeviceRead(spng_ctx *ctx, void *user, void *dst, size_t length)
{
    Q_UNUSED(ctx);

    QIODevice *dev = reinterpret_cast<QIODevice*>(user);
    if (dev->atEnd()) {
        return SPNG_IO_EOF;
    }

    const qint64 amountRead = dev->read((char*)dst, length);
    if (amountRead == -1) {
        qCDebug(LOGGING) << "IO error" << dev->errorString();
        return SPNG_IO_ERROR;
    }

    if (amountRead < length) {
        return SPNG_IO_EOF;
    }

    return 0;
}

class QSpngHandler : public QImageIOHandler
{
public:
    explicit QSpngHandler(QIODevice *device) : m_device(device) {
        setFormat("png");
    }

    virtual ~QSpngHandler() {
        if(m_ctx) {
            spng_ctx_free(m_ctx);
        }
    }

    bool supportsOption(ImageOption option) const override {
        switch(option) {
        case Description:
            return true;
        case Size:
            return true;
        case Animation: // todo: this is one of few libraries with proper apng support
        default:
            return false;
        }
    }

    QVariant option(ImageOption option) const override {
        QSpngHandler *that = const_cast<QSpngHandler*>(this);
        switch(option) {
        case Size:
            if (!that->ensureInitialized()) {
                return QVariant();
            }
            return QSize(m_header.width, m_header.height);
        case Description:
            if (m_description) {
                return *m_description;
            }
            if (!that->ensureInitialized()) {
                return QVariant();
            }
            that->m_description.reset(new QString);
            for (const spng_text &text : readTexts()) {
                QString value = QString::fromUtf8(text.text, text.length).trimmed();
                QString key = QString::fromUtf8(text.keyword).trimmed();
                if (!m_description->isEmpty()) {
                    that->m_description->append("\n\n");
                }
                key.remove(':'); // qimagereader is so broken...
                that->m_description->append(key + ": " + value);
            }
            return *m_description;
        default:
            return QVariant();
        }
    }

    static inline bool isPNG(const QByteArray &buffer) {
        return buffer.startsWith("\x89\x50\x4E\x47\x0D\x0A\x1A\x0A");
    }

    static bool probe(QIODevice *device) {
        QByteArray buffer = device->peek(8);
        if (buffer.size() == 8) {
            return isPNG(buffer);
        }
        if (device->isSequential()) {
            qCWarning(LOGGING) << "Sequential device";
            return false;
        }
        const qint64 originalPos = device->pos();
        buffer = device->read(8);
        device->seek(originalPos);
        return isPNG(buffer);
    }

    bool canRead() const override {
        return true; // HACK: we can't actually trust the QDevice, because QImageReader is a piece of shit
    }

    bool read(QImage *image) override {
        if (!ensureInitialized()) {
            return false;
        }
        QElapsedTimer t; t.start();

        bool native = false;
        const QImage::Format format = imageFormat(&native);
        if (format == QImage::Format_Invalid) {
            qCWarning(LOGGING) << "Failed to find format";
            return false;
        }

        int bytesPerLine = m_header.width;
        spng_format decodeFormat{};
        spng_decode_flags flags{};
        if (native) {
            switch(format) {
            case QImage::Format_RGBA64:
                decodeFormat = SPNG_FMT_PNG; // _PNG instead of _RAW means to convert endianness for us
                bytesPerLine = m_header.width * 8;
                break;
            case QImage::Format_RGBX64:
                // TODO
                // Qt has no 3x16bit format,
                // so let spng expand with a dummy channel
                decodeFormat = SPNG_FMT_RGBA16;
                bytesPerLine = m_header.width * 8;
                break;
            case QImage::Format_RGBA8888:
                decodeFormat = SPNG_FMT_PNG;
                bytesPerLine = m_header.width * 4;
                break;
            case QImage::Format_RGB888:
                decodeFormat = SPNG_FMT_PNG;
                bytesPerLine = m_header.width * 3;
                break;
            case QImage::Format_Grayscale16:
                decodeFormat = SPNG_FMT_PNG;
                bytesPerLine = m_header.width * 2;
                break;
            case QImage::Format_Grayscale8:
                decodeFormat = SPNG_FMT_PNG;
                bytesPerLine = m_header.width * 1;
                break;
            case QImage::Format_Indexed8:
                decodeFormat = SPNG_FMT_PNG;
                bytesPerLine = m_header.width * 1;
                break;
            case QImage::Format_Mono:
                decodeFormat = SPNG_FMT_PNG;
                bytesPerLine = qCeil(m_header.width / 8.);
                break;
            default:
                qCWarning(LOGGING) << "unhandled format" << image->format();
                return false;
            }
        } else {
            if (m_header.bit_depth == 16) {
                decodeFormat = SPNG_FMT_RGBA16;
                bytesPerLine = m_header.width * 8;
            } else {
                decodeFormat = SPNG_FMT_RGBA8;
                bytesPerLine = m_header.width * 4;
            }
            flags = SPNG_DECODE_TRNS;
        }


        size_t bufferSize = 0;
        int ret = spng_decoded_image_size(m_ctx, decodeFormat, &bufferSize);
        if (ret != SPNG_OK) {
            qCWarning(LOGGING) << "Failed to get decoded image size" << spng_strerror(ret) << decodeFormat << m_header.color_type;
            return false;
        }

        qCDebug(LOGGING) << "Bytes out" << bufferSize << "format from spng" << decodeFormat << "our format" << format;
        const size_t expectedSize = bytesPerLine * m_header.height;
        if (bufferSize != expectedSize) {
            qCWarning(LOGGING) << "Invalid output size, got" << bufferSize << "bytes, but dimensions is" << bytesPerLine << "x" << m_header.height << "which is" << expectedSize;
            return false;
        }

        void *buffer = malloc(bufferSize);
        ret = spng_decode_image(m_ctx, buffer, bufferSize, decodeFormat, flags);
        if (ret != SPNG_OK) {
            qCWarning(LOGGING) << "Failed to decode image, spng error:" << spng_strerror(ret);
            free(buffer);
            return false;
        }

        *image = QImage((uchar*)buffer, m_header.width, m_header.height, bytesPerLine, format, free, buffer);
        if (image->isNull()) {
            qCWarning(LOGGING) << "QImage parameter check failed, out size" << bufferSize << "bytes, dimensions" << bytesPerLine << "x" << m_header.height << format;
            free(buffer);
            return false;
        }

        if (format == QImage::Format_Indexed8) {
            image->setColorTable(readColorTable());
        }

        const QColorSpace colorspace = readColorSpace();
        if (colorspace.isValid()) {
            image->setColorSpace(colorspace);
        }

        const bool updateDescription = m_description == nullptr;
        if (updateDescription) {
            m_description.reset(new QString);
        }

        for (const spng_text &text : readTexts()) {
            QString value = QString::fromUtf8(text.text, text.length).trimmed();
            QString key = QString::fromUtf8(text.keyword).trimmed();
            image->setText(key, value);

            if (updateDescription) {
                if (!m_description->isEmpty()) {
                    m_description->append("\n\n");
                }
                m_description->append(key + ": " + value);
            }
        }

        static const bool benchmark = qEnvironmentVariableIsSet("QSPNG_BENCHMARK");
        if (benchmark && t.elapsed()) {
            qCDebug(LOGGING) << "Image read in" << t.elapsed();
        }

        if (format == QImage::Format_Mono) {
            image->setColorCount(2);
            image->setColor(0, qRgb(0, 0, 0));
            image->setColor(1, qRgb(255, 255, 255));
        }

        return true;
    }

private:
    QImage::Format imageFormat(bool *native = nullptr) const {
        if (!const_cast<QSpngHandler*>(this)->ensureInitialized()) {
            return QImage::Format_Invalid;
        }
        if (native) {
            *native = true;
        }

        qCDebug(LOGGING) << "Bit depth" << m_header.bit_depth << "color type" << spng_color_type(m_header.color_type);

        switch(m_header.bit_depth) {
        case 16:
            if (m_header.color_type == SPNG_COLOR_TYPE_TRUECOLOR_ALPHA) {
                return QImage::Format_RGBA64;
            } else if (m_header.color_type == SPNG_COLOR_TYPE_TRUECOLOR) {
                return QImage::Format_RGBX64;
            } else if (m_header.color_type == SPNG_COLOR_TYPE_GRAYSCALE) {
                return QImage::Format_Grayscale16;
            }
            break;
        case 8:
            if (m_header.color_type == SPNG_COLOR_TYPE_TRUECOLOR_ALPHA) {
                return QImage::Format_RGBA8888;
            } else if (m_header.color_type == SPNG_COLOR_TYPE_TRUECOLOR) {
                return QImage::Format_RGB888;
            } else if (m_header.color_type == SPNG_COLOR_TYPE_INDEXED) {
                return QImage::Format_Indexed8;
            } else if (m_header.color_type == SPNG_COLOR_TYPE_GRAYSCALE) {
                return QImage::Format_Grayscale8;
            }
            break;
        case 2:
            // What the fuck png
            break;
        case 1: // should be indexed?
            return QImage::Format_Mono;
        default:
            break;
        }

        qCDebug(LOGGING) << "Unhandled bit depth" << m_header.bit_depth << "or color type" << spng_color_type(m_header.color_type);

        if (native) {
            *native = false;
        }

        if (m_header.bit_depth == 16) {
            return QImage::Format_RGBA64;
        } else {
            return QImage::Format_RGBA8888;
        }
    }

    QVector<spng_text> readTexts() const {
        uint32_t numTexts = 0;
        int ret = spng_get_text(m_ctx, nullptr, &numTexts); // get number of items
        if (ret != SPNG_OK) {
            if (ret != SPNG_ECHUNKAVAIL) {
                qCWarning(LOGGING) << "Failed to get text item count" << spng_strerror(ret);
            }
            return {};
        }
        QVector<spng_text> texts(numTexts);
        ret = spng_get_text(m_ctx, texts.data(), &numTexts);
        if (ret != SPNG_OK) {
            qCWarning(LOGGING) << "Failed to get text items" << spng_strerror(ret);
            return {};
        }
        return texts;
    }

    QColorSpace readColorSpace() {
        QColorSpace colorspace;

        spng_iccp iccProfile{};
        int ret = spng_get_iccp(m_ctx, &iccProfile);
        if (ret == SPNG_OK) {
            colorspace = QColorSpace::fromIccProfile(QByteArray(iccProfile.profile, iccProfile.profile_len));
            if (colorspace.isValid()) {
                return colorspace;
            } else {
                qCWarning(LOGGING) << "Invalid colorspace decoded";
            }
        }

        double gamma = 0;
        ret = spng_get_gama(m_ctx, &gamma);
        if (ret != SPNG_OK || !gamma) {
            if (ret != SPNG_ECHUNKAVAIL) {
                qCWarning(LOGGING) << "Failed to read gamma" << gamma << spng_strerror(ret);
            }
            return colorspace;
        }
        gamma = 1. / gamma;

        spng_chrm chrom{};
        ret = spng_get_chrm(m_ctx, &chrom);
        if (ret != SPNG_OK) {
            qCDebug(LOGGING) << "Failed to decode chromacities:" << spng_strerror(ret);
            return QColorSpace(QColorSpace::Primaries::SRgb, QColorSpace::TransferFunction::Linear, gamma);
        }

        return QColorSpace(
                {chrom.white_point_x, chrom.white_point_y},
                {chrom.red_x, chrom.red_y},
                {chrom.green_x, chrom.green_y},
                {chrom.blue_x, chrom.blue_y},
                QColorSpace::TransferFunction::Gamma,
                gamma
            );
    }
    QVector<QRgb> readColorTable() {
        QVector<QRgb> colortable(256);

        spng_plte plte;
        int ret = spng_get_plte(m_ctx, &plte);
        if (ret != SPNG_OK) {
            qCWarning(LOGGING) << "Failed to read palette" << spng_strerror(ret) << "entries" << plte.n_entries;
            for (int i=0;i<256; i++) {
                colortable[i] = qRgb(i, i, i);
            }
            return colortable;
        }

        static_assert(sizeof(plte.entries)/sizeof(plte.entries[0]) == 256);

        int i = 0;
        spng_trns transparency;
        ret = spng_get_trns(m_ctx, &transparency);
        if (ret == SPNG_OK && transparency.n_type3_entries < 256) {
            for (i=0; i<transparency.n_type3_entries; i++) {
                colortable[i] = qRgba(
                        plte.entries[i].red,
                        plte.entries[i].green,
                        plte.entries[i].blue,
                        transparency.type3_alpha[i]
                        );
            }
        }
        if (plte.n_entries != 256) {
            qCDebug(LOGGING) << "Weird number of palette entries" << plte.n_entries;
        }

        for (; i<256; i++) {
            colortable[i] = qRgb(plte.entries[i].red, plte.entries[i].green, plte.entries[i].blue);
        }

        return colortable;
    }

    bool ensureInitialized() {
        QElapsedTimer t; t.start();
        static const bool benchmark = qEnvironmentVariableIsSet("QSPNG_BENCHMARK");

        if (m_initialized) {
            return true;
        }

        m_ctx = spng_ctx_new(0);
        if (!m_ctx) {
            qCWarning(LOGGING) << "Failed to initialize context";
            return false;
        }

        // somewhat arbitrary (from spng test sources).
        // QImage is limited by MAX_INT - 32, and total byte size.
        int ret = spng_set_image_limits(m_ctx, 200000, 200000);
        if (ret != SPNG_OK) {
            qCWarning(LOGGING) << "Failed to set image limits" << spng_strerror(ret);
            return false;
        }

        spng_set_png_stream(m_ctx, qiodeviceRead, m_device);

        if (benchmark && t.elapsed()) qCDebug(LOGGING) << "Set stream in" << t.restart();

        ret = spng_get_ihdr(m_ctx, &m_header);
        if (ret != SPNG_OK) {
            qCWarning(LOGGING) << "Failed to get ihdr" << spng_strerror(ret);
            return false;
        }

        m_initialized = true;

        if (benchmark) qCDebug(LOGGING) << "Initialized in" << t.restart() << "ms";

        return true;
    }

    QScopedPointer<QString> m_description;
    QIODevice *m_device = nullptr;
    bool m_initialized = false;
    spng_ctx *m_ctx = nullptr;
    spng_ihdr m_header{};
};

QImageIOPlugin::Capabilities QSpngPlugin::capabilities(QIODevice *device, const QByteArray &format) const
{
    qCDebug(LOGGING) << "Queried for capabilities";
    if (device && device->isReadable()) {
        if(QSpngHandler::probe(device)) {
            return CanRead;
        }
    } else if (format == "png") {
        return CanRead;
    }

    return Capabilities();
}

QImageIOHandler *QSpngPlugin::create(QIODevice *device, const QByteArray &format) const
{
    qCDebug(LOGGING) << "Queried for create";
    if (!(capabilities(device, format) & CanRead)) {
        qCWarning(LOGGING) << "Asked to create for file we can't read";
    }
    QFileDevice *file = qobject_cast<QFileDevice*>(device);
    if (file) {
        qCDebug(LOGGING) << "Opening" << file->fileName();
    }
    return new QSpngHandler(device);
}
