#pragma once

#include <QImageIOPlugin>
#include <QLoggingCategory>

class QSpngPlugin: public QImageIOPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QImageIOHandlerFactoryInterface" FILE "extensions.json")

public:
    Capabilities capabilities(QIODevice *device, const QByteArray &format) const override;
    QImageIOHandler *create(QIODevice *device, const QByteArray &format) const override;
};

Q_DECLARE_LOGGING_CATEGORY(LOGGING);
